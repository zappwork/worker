package worker

import "reflect"

// NewQueueItem creates a new queue item to be processed, here
// f is the routine func and args is the optional arguments
// for running the routine.
func NewQueueItem(f interface{}, args ...interface{}) *QueueItem {
	return &QueueItem{f: f, args: args}
}

// QueueItem for add in an function to execute with
// the specified arguments
type QueueItem struct {
	f    interface{}
	args []interface{}
}

// Run the queue item
func (q QueueItem) Run() {
	fnc := reflect.ValueOf(q.f)
	var rargs []reflect.Value
	for _, a := range q.args {
		rargs = append(rargs, reflect.ValueOf(a))
		// fmt.Println(arv)
	}
	fnc.Call(rargs)
}
