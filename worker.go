// Package worker Simple Queue & Concurrency Manager
// If you want control over the number of concurrent process which are running. This package is perfect for you. It is a simple routing queue and concurrency manager. Add routines to a queue and they will processes in the max concurrent routines available to the worker.
// The package is ideal for processing information or heavy routines separately of the main process and not put to much stress on the main application or external media such as mysql, disks, etc..
//
// Run a worker as a single application
//
//	package main
//	import (
// 		"log"
//		"time"
//		"bitbucket.org/zappwork/worker"
//	)
//	func main() {
//		w, err := worker.NewWorker(10, false, true)
//		if err != nil {
// 			log.Fatal(err)
//		}
//		for i := 0; i < 100; i++ {
//			w.Add(f, i)
//		}
//		w.Start()
//	}
//	func f(i int) {
//		time.Sleep(time.Duration(1) * time.Second)
//		log.Printf("Ran %03d\n", i)
//	}
//
// Running a separate worker
//
//	package main
//	import (
//		"fmt"
//		"log"
//		"net/http"
//		"time"
//		"bitbucket.org/zappwork/worker"
//	)
//	var (
//		work *worker.Worker
//		ran  int
//	)
//	func main() {
//		w, err := worker.NewWorker(10, true, false)
//		if err != nil {
//			log.Fatal(err)
//		}
//		work = w
//		http.HandleFunc("/", working)
//		fmt.Println("http server listening add items by calling http://localhost:9090/")
//		err = http.ListenAndServe(":9090", nil)
//		if err != nil {
//			log.Fatal(err)
//		}
//	}
//	func working(w http.ResponseWriter, r *http.Request) {
//		ran = ran + 1
//		work.Add(f, ran, r.URL.Path)
//		fmt.Fprintf(w, "Added item")
//	}
//	func f(i int, path string) {
//		time.Sleep(time.Duration(100) * time.Millisecond)
//		log.Printf("Ran %03d %s\n", i, path)
//	}
package worker

import (
	"fmt"
	"sync"
)

// NewWorker creates a new Worker object
func NewWorker(maxConcurrency int, start, wait bool) (*Worker, error) {
	if start && wait {
		return nil, fmt.Errorf("Cannot start and wait, please pick one or neither")
	}
	w := &Worker{numConcurrentWorkers: maxConcurrency}
	// if wait set the waitgroup
	if wait {
		w.stop = make(chan bool)
	}
	// Start when requesting auto start
	if start {
		w.Start()
	}
	return w, nil
}

// Worker a worker object
type Worker struct {
	numConcurrentWorkers int
	queue                []*QueueItem
	rwMutex              sync.RWMutex
	runningWorkers       int
	running              bool
	stop                 chan bool
}

// NumConcurrentWorker get the number of concurrent workers
func (w *Worker) NumConcurrentWorker() int {
	return w.numConcurrentWorkers
}

// SetNumConcurrentWorker set the number of concurrent workers
func (w *Worker) SetNumConcurrentWorker(c int) {
	w.numConcurrentWorkers = c
}

// Add a new routing to the worker, here
// f is the routine func and args is the optional arguments
// for running the routine.
func (w *Worker) Add(f interface{}, args ...interface{}) {
	w.AddItem(&QueueItem{f: f, args: args})
}

// AddItem adds an item to the queue to be processed
func (w *Worker) AddItem(i *QueueItem) {
	w.rwMutex.Lock()
	w.queue = append(w.queue, i)
	w.rwMutex.Unlock()
	w.checkQueue()
}

// Start running the worker
func (w *Worker) Start() {
	// Do not run the worker when there are no queue items
	if w.running {
		return
	}
	w.running = true

	// Starts when the items are added to the queue
	if len(w.queue) == 0 {
		return
	}
	// run the check queue to run queue items
	w.checkQueue()

	// Should we wait to stop?
	if w.stop != nil {
		<-w.stop
	}
}

// Stop running the worker when the current items are finished
func (w *Worker) Stop(waitTillfinished bool) bool {
	w.running = false
	if !waitTillfinished {
		return true
	}
	for w.runningWorkers > 0 {
		// wait till all workers are finished
	}
	return true
}

// Check the
func (w *Worker) checkQueue() {
	// If not running do nothing
	if !w.running {
		return
	}

	// Only add runner if the number of running is less
	// than the number of concurrent workers allowed
	if w.runningWorkers < w.numConcurrentWorkers {
		w.rwMutex.Lock()
		for len(w.queue) > 0 && w.runningWorkers < w.numConcurrentWorkers {
			// get first item from the queue
			i := w.queue[0]
			if len(w.queue) == 1 {
				w.queue = make([]*QueueItem, 0)
			} else {
				w.queue = w.queue[1:]
			}
			// increase the number or running workers
			w.runningWorkers = w.runningWorkers + 1
			go w.run(i)
		}
		w.rwMutex.Unlock()
	}

	// Signal stop when worker is finished when waiting
	if w.stop != nil && len(w.queue) == 0 && w.runningWorkers == 0 {
		w.stop <- true
	}
}

// Handle an item in the queue
func (w *Worker) run(i *QueueItem) {
	// Handle the queue item and when finished
	i.Run()

	// decrease the number of running workers
	w.rwMutex.Lock()
	w.runningWorkers = w.runningWorkers - 1
	w.rwMutex.Unlock()

	// check the queue
	w.checkQueue()
}
