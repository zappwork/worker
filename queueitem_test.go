package worker

import (
	"fmt"
	"reflect"
	"testing"
)

func testFunc(s string) {
	if s != "" {
		fmt.Println(s)
	}
}

func TestQueueItem(t *testing.T) {
	w, err := NewWorker(1, false, false)
	if err != nil {
		t.Errorf("Worker error %s", err)
	}
	w.Add(testFunc, "")
	w.AddItem(NewQueueItem(testFunc, ""))
	if !reflect.DeepEqual(w.queue[0].args, w.queue[1].args) {
		t.Errorf("QueueItems not equal %+v vs %+v", w.queue[0], w.queue[1])
	}
}
