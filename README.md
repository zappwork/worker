# Simple Queue & Concurrency Manager #
If you want control over the number of concurrent process which are running. This package is perfect for you. It is a simple routing queue and concurrency manager. Add routines to a queue and they will processes in the max concurrent routines available to the worker.

The package is ideal for processing information or heavy routines separately of the main process and not put to much stress on the main application or external media such as mysql, disks, etc..

[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/zappwork/worker)](https://goreportcard.com/report/bitbucket.org/zappwork/worker)
[![GoDoc](https://godoc.org/bitbucket.org/zappwork/worker?status.svg)](https://godoc.org/bitbucket.org/zappwork/worker)

## Installation ##
Install by using the following command.

```bash
go get bitbucket.org/zappwork/worker
```

## Running a single worker ##
The worker is ideal for running a few routines and then stop the application. See the next example how to do this.

```go
package main

import (
	"log"
	"time"

	"bitbucket.org/zappwork/worker"
)

func main() {
    // Create a new worker and set wait to true
	w, err := worker.NewWorker(10, false, true)
	if err != nil {
		log.Fatal(err)
	}
    // Add 100 routines
	for i := 0; i < 100; i++ {
		w.Add(f, i)
	}
    // Start the worker
	w.Start()
}

// Simple echo func
func f(i int) {
	time.Sleep(time.Duration(1) * time.Second)
	log.Printf("Ran %03d\n", i)
}
```

## Running a seperate worker ##
This example shows how to run a worker as a separate process.

```go
package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/zappwork/worker"
)

var (
	work *worker.Worker
	ran  int
)

func main() {
	w, err := worker.NewWorker(10, true, false)
	if err != nil {
		log.Fatal(err)
	}
	work = w
	http.HandleFunc("/", working)
	fmt.Println("http server listening add items by calling http://localhost:9090/")
	err = http.ListenAndServe(":9090", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func working(w http.ResponseWriter, r *http.Request) {
	ran = ran + 1
	work.Add(f, ran, r.URL.Path)
	fmt.Fprintf(w, "Added item")
}

func f(i int, path string) {
	time.Sleep(time.Duration(100) * time.Millisecond)
	log.Printf("Ran %03d %s\n", i, path)
}
```
