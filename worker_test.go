package worker

import (
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestNewWorker(t *testing.T) {
	w, err := NewWorker(5, false, false)
	if err != nil {
		t.Errorf("Worker error %s", err)
	}
	want := &Worker{
		numConcurrentWorkers: 5,
	}
	if !reflect.DeepEqual(w, want) {
		t.Errorf("NewWorker not correct %+v vs %+v", w, want)
	}
}

func TestNewWorkerError(t *testing.T) {
	_, err := NewWorker(5, true, true)
	if err == nil {
		t.Error("Worker should give error when start and wait are true")
	}
}

func TestConcurrent(t *testing.T) {
	w, err := NewWorker(5, false, false)
	if err != nil {
		t.Errorf("Worker error %s", err)
	}
	w.SetNumConcurrentWorker(10)
	want := &Worker{
		numConcurrentWorkers: 10,
	}
	if w.NumConcurrentWorker() != want.NumConcurrentWorker() {
		t.Errorf("Num concurrent not correct %d vs %d", w.NumConcurrentWorker(), want.NumConcurrentWorker())
	}
}

type Test struct {
	Mux   sync.RWMutex
	Items int
}

func testHandler(i *Test) {
	i.Mux.Lock()
	i.Items = i.Items + 1
	i.Mux.Unlock()
	time.Sleep(time.Duration(200) * time.Millisecond)
}

func TestRoutines(t *testing.T) {
	w, err := NewWorker(5, false, true)
	if err != nil {
		t.Errorf("Worker error %s", err)
	}
	w.Start()
	w.Stop(false)
	var s Test
	for i := 0; i < 10; i++ {
		w.Add(testHandler, &s)
	}
	if len(w.queue) < 10 {
		t.Errorf("Not all routines added to queue %d", len(w.queue))
	}
	w.Start()
	w.Stop(true)
	if s.Items != 10 {
		t.Errorf("Did not run all routines %d", s.Items)
	}
}

func TestAutostart(t *testing.T) {
	w, err := NewWorker(10, true, false)
	if err != nil {
		t.Errorf("Worker error %s", err)
	}
	var s Test
	for i := 0; i < 10; i++ {
		w.Add(testHandler, &s)
	}
	w.Stop(true)
	if s.Items != 10 {
		t.Errorf("Did not run all routines %d", s.Items)
	}
}
